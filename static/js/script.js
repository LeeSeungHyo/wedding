var latlng = new daum.maps.LatLng(35.20810982008988, 126.8221104257521);
var map = new daum.maps.Map(document.getElementById('map'), {
    center: latlng,
    level: 4,
    draggable: false,
    disableDoubleClickZoom: false,
});

var marker = new daum.maps.Marker({
    position: latlng,
    map: map,
});

daum.maps.event.addListener(marker, 'click', function(mouseEvent) {
});

var s = skrollr.init({
    forceHeight: false,
    mobileCheck: function () {
        return false;
    },
});

$(document).on('keydown', function (e) {
    if (e.which == 32) {
        return false;
    }
});

Kakao.init('ac1103745fb2a2be4431122d12260cc6');
Kakao.Link.createDefaultButton({
  container: '#kakao-link-btn',
  objectType: 'feed',
  content: {
          title: '이승효 & 임보람, 저희 결혼합니다.',
          imageUrl: 'https://s26.postimg.org/y48iexzop/hyoram.jpg',
          link: {
            mobileWebUrl: 'http://www.hyoram.cf',
            webUrl: 'http://www.hyoram.cf'
          }
        },
  buttons: [{
    title: '모바일 청첩장 보기',
    link: {
      mobileWebUrl: 'http://www.hyoram.cf',
      webUrl: 'http://www.hyoram.cf'
    }
  }]
});

// twitter
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');